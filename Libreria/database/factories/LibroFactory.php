<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Libro>
 */
class LibroFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        
        return [
            'titulo' => fake()->sentence(3),
            'editorial_id' => fake()->numberBetween(1, 100), 
            'edicion' => fake()->numberBetween(1, 10),
            'pais' => substr(fake()->country, 0, 50),
            'precio' => fake()->randomFloat(2, 10, 1000),
        ];
    }
}
