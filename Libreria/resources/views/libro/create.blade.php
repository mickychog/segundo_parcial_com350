<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <form action="{{ route('libro.store') }}" method="POST">
        @csrf
        <div>
            <label for="titulo">Título</label>
            <input type="text" name="titulo" id="titulo">
        </div>
        <div>
            <label for="editorial_id">Editorial ID</label>
            <input type="number" name="editorial_id" id="editorial_id">
        </div>
        <div>
            <label for="edicion">Edición</label>
            <input type="number" name="edicion" id="edicion">
        </div>
        <div>
            <label for="pais">País</label>
            <input type="text" name="pais" id="pais">
        </div>
        <div>
            <label for="precio">Precio</label>
            <input type="number" name="precio" id="precio" step="0.01">
        </div>
        <div>
            <input type="submit" value="Registrar">
        </div>
    </form>
    
    
</body>
</html>