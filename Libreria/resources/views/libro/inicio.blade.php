@extends('layouts.app')

@section('content')
<h1>Listado de editoriales</h1>
<div class="flex flex-col">
    <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
            <div class="overflow-hidden">
                <table class="min-w-full text-left text-sm font-light">
                    <thead class="border-b font-medium dark:border-neutral-500">
                        <tr>
                            <th scope="col" class="px-6 py-4">Nombre</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($editoriales as $editorial)
                        <tr class="border-b dark:border-neutral-500">
                            <td class="whitespace-nowrap px-6 py-4">{{ $editorial->nombre }}</td>
                            <td>
                                <a href="{{ route('editorial.show', $editorial->id) }}">Mostrar</a>
                                <a href="{{ route('editorial.edit', $editorial->id) }}">Editar</a>
                            </td>
                            <td>
                                <form action="{{ route('editorial.destroy', $editorial->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<a href="{{ route('editorial.create') }}">Registrar Editorial</a>

@endsection