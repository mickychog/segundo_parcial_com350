<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <ul>
        <li>Título: {{ $libro->titulo }}</li>
        <li>Editorial ID: {{ $libro->editorial_id }}</li>
        <li>Edición: {{ $libro->edicion }}</li>
        <li>País: {{ $libro->pais }}</li>
        <li>Precio: {{ $libro->precio }}</li>
    </ul>
    
    <meta http-equiv="refresh" content="3; url={{ route('libro.index') }}" />
    
</body>
</html>