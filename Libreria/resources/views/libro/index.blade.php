@extends('layouts.app')

@section('content')
<h1>Listado de libros</h1>
<div class="flex flex-col">
    <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="inline-block min-w-full py-2 sm:px-6 lg:px-8">
            <div class="overflow-hidden">
                <table class="min-w-full text-left text-sm font-light">
                    <thead class="border-b font-medium dark:border-neutral-500">
                        <tr>
                            <th scope="col" class="px-6 py-4">Título</th>
                            <th scope="col" class="px-6 py-4">Editorial</th>
                            <th scope="col" class="px-6 py-4">Edición</th>
                            <th scope="col" class="px-6 py-4">Paígits</th>
                            <th scope="col" class="px-6 py-4">Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($libros as $libro)
                        <tr class="border-b dark:border-neutral-500">
                            <td class="whitespace-nowrap px-6 py-4">{{ $libro->titulo }}</td>
                            <td class="whitespace-nowrap px-6 py-4">{{ $libro->editorial_id}}</td>
                            <td class="whitespace-nowrap px-6 py-4">{{ $libro->edicion }}</td>
                            <td class="whitespace-nowrap px-6 py-4">{{ $libro->pais }}</td>
                            <td class="whitespace-nowrap px-6 py-4">{{ $libro->precio }}</td>
                            <td>
                                <a href="{{ route('libro.show', $libro->id) }}">Mostrar</a>
                                <a href="{{ route('libro.edit', $libro->id) }}">Editar</a>
                            </td>
                            <td>
                                <form action="{{ route('libro.destroy', $libro->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<a href="{{ route('libro.create') }}">Registrar Libro</a>

@endsection