<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <h1>Listado de libros con {{ $tipo_documento->descripcion }}</h1>
    <table>
        <tr>
            <th>Título</th>
            <th>Editorial</th>
            <th>Edición</th>
            <th>País</th>
            <th>Precio</th>
        </tr>
        @foreach ($libros as $libro)
        <tr>
            <td>{{ $libro->titulo }}</td>
            <td>{{ $libro->editorial_id }}</td>
            <td>{{ $libro->edicion }}</td>
            <td>{{ $libro->pais }}</td>
            <td>{{ $libro->precio }}</td>
            <td>
                <a href="{{ route('libro.show', $libro->id) }}">Mostrar</a>
                <a href="{{ route('libro.edit', $libro->id) }}">Editar</a>
            </td>
            <td>
                <form action="{{ route('libro.destroy', $libro->id) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    
    <a href="{{ route('libro.create') }}">Registrar Libro</a>
    
</body>
</html>