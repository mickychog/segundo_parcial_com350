const mongoose = require('mongoose');

const EditorialSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: 'Nombre de la editorial es requerido'
    },
    direccion: String, 
    telefono: String,
    libros: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Libro' 
        }
    ],
});

module.exports = mongoose.model('Editorial', EditorialSchema);
