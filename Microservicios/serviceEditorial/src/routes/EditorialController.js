var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var Editorial = require('../models/Editorial'); 
var logger = require('../config/log');

// Crear una editorial
router.post('/', function(req, res) {
    logger.info("Begin Create Editorial");

    Editorial.create({
        nombre: req.body.nombre,
        direccion: req.body.direccion,
        telefono: req.body.telefono
        
    }, function(err, editorial) {
        if (err) return res.status(500).send(err.message);
        res.status(200).send(editorial);
        logger.info("End Create Editorial");
    });
});

// Listar todas las editoriales
router.get('/', function(req, res) {
    logger.info("Begin List Editoriales");

    Editorial.find({}, function(err, editoriales) {
        if (err) return res.status(500).send("There was a problem finding the editorials.");
        res.status(200).send(editoriales);
        logger.info("End List Editoriales");
    });
});

// Obtener una editorial por ID
router.get('/:id', function(req, res) {
    logger.info("Begin Get Editorial");

    Editorial.findById(req.params.id, function(err, editorial) {
        if (err) return res.status(500).send("There was a problem finding the editorial.");
        if (!editorial) return res.status(404).send("No editorial found.");
        res.status(200).send(editorial);
        logger.info("End Get Editorial");
    });
});

// Actualizar una editorial por ID
router.put('/:id', function(req, res) {
    logger.info("Begin Update Editorial");

    Editorial.findByIdAndUpdate(req.params.id, req.body, { new: true }, function(err, editorial) {
        if (err) return res.status(500).send("There was a problem updating the editorial.");
        res.status(200).send(editorial);
        logger.info("End Update Editorial");
    });
});

// Eliminar lógicamente una editorial por ID
router.delete('/:id', function(req, res) {
    logger.info("Begin Inactive Editorial");

    Editorial.findByIdAndUpdate(req.params.id, { state: "inactive" }, { new: true }, function(err, editorial) {
        if (err) return res.status(500).send("There was a problem inactivating the editorial.");
        res.status(200).send(editorial);
        logger.info("End Inactive Editorial");
    });
});

module.exports = router;
