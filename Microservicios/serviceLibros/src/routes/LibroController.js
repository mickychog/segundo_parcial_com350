var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var Libro = require('../models/Libro'); 
var logger = require('../config/log');

// Crear un libro
router.post('/', function(req, res) {
    logger.info("Begin Create Libro");

    Libro.create({

    }, function(err, libro) {
        if (err) return res.status(500).send(err.message);
        res.status(200).send(libro);
        logger.info("End Create Libro");
    });
});

// Listar todos los libros
router.get('/', function(req, res) {
    logger.info("Begin List Libros");
    
    Libro.find({}, function(err, libros) {
        if (err) return res.status(500).send("There was a problem finding the books.");
        res.status(200).send(libros);
        logger.info("End List Libros");
    });
});

// Obtener un libro por ID
router.get('/:id', function(req, res) {
    logger.info("Begin Get Libro");
   
    Libro.findById(req.params.id, function(err, libro) {
        if (err) return res.status(500).send("There was a problem finding the book.");
        if (!libro) return res.status(404).send("No book found.");
        res.status(200).send(libro);
        logger.info("End Get Libro");
    });
});

// Actualizar un libro por ID
router.put('/:id', function(req, res) {
    logger.info("Begin Update Libro");
   
    Libro.findByIdAndUpdate(req.params.id, req.body, { new: true }, function(err, libro) {
        if (err) return res.status(500).send("There was a problem updating the book.");
        res.status(200).send(libro);
        logger.info("End Update Libro");
    });
});

// Eliminar lógicamente un libro por ID
router.delete('/:id', function(req, res) {
    logger.info("Begin Inactive Libro");
    
    Libro.findByIdAndUpdate(req.params.id, { state: "inactive" }, { new: true }, function(err, libro) {
        if (err) return res.status(500).send("There was a problem inactivating the book.");
        res.status(200).send(libro);
        logger.info("End Inactive Libro");
    });
});

module.exports = router;
