var mongoose = require('mongoose');

const LibroSchema = new mongoose.Schema({
    titulo: {
        type: String,
        required: true 
    },
    editorial: {
        type: String,
        required: true 
    },
    edicion: Number,
    pais: String,
    precio: Number,
    autor: [{
        nombre: String,
        apellido: String,
        email: String
    }],
});

module.exports = mongoose.model('Libro', LibroSchema);
